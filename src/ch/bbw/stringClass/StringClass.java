package ch.bbw.stringClass;

import java.util.Scanner;

public class StringClass {
	public StringClass() {
		Scanner sc = new Scanner(System.in);
		String file = sc.nextLine();
		String out;
		if (file.contains(".")){
			out = file.substring(0, file.indexOf("."));
			System.out.println("Der Name der Datei lautet: " + out);
		}
	}
}
